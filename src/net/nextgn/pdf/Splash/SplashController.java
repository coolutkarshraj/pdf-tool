/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nextgn.pdf.Splash;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author UKa78668
 */
public class SplashController implements Initializable {
    
    private Label label;
    
    @FXML
    private AnchorPane acnchorPane;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        new SplashScreen().start();
    } 
    
    class SplashScreen extends Thread {
        @Override
        public void run(){
            try {
                Thread.sleep(5000);
                
                Platform.runLater(new Runnable(){
                    @Override
                    public void run() {
                       Parent root = null;
                       
                       try {
                           //root = FXMLLoader.load(getClass().getResource("../Login/Login.fxml"));
                           root =FXMLLoader.load(getClass().getResource("/net/nextgn/pdf/Login/Login.fxml"));
                       } catch(IOException ex) {
                           //
                       }
                       
                       Scene scene = new Scene(root);
                       Stage stage = new Stage();
                       stage.setScene(scene);
                       stage.initStyle(StageStyle.UNDECORATED);
                       stage.show();
                       
                       acnchorPane.getScene().getWindow().hide();
                    }
                    
                });
                
            } catch (InterruptedException ex){
                // Logger.getLogger(name, resourceBundleName)
            }
        }
    }
    
}
