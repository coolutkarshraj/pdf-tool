/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nextgn.pdf.component;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javafx.application.HostServices;
import javafx.scene.control.Alert;
import rst.pdfbox.layout.elements.Document;
import rst.pdfbox.layout.elements.Frame;
import rst.pdfbox.layout.elements.Paragraph;
import rst.pdfbox.layout.elements.render.VerticalLayoutHint;
import rst.pdfbox.layout.shape.Rect;
import rst.pdfbox.layout.shape.RoundRect;
import rst.pdfbox.layout.shape.Stroke;
import rst.pdfbox.layout.text.Alignment;
import rst.pdfbox.layout.text.BaseFont;
import rst.pdfbox.layout.text.Position;

/**
 *
 * @author NEXTGN001
 */
public class PdfGenerator {

    public static String Text1 = "Contrary to popular belief, Lorem Ipsum is not simply random text. "
            + "It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old."
            + " Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,"
            + " looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, "
            + "and going through the cites of the word in classical literature, discovered the undoubtable source."
            + " Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" "
            + "(The Extremes of Good and Evil) by Cicero, written in 45 BC. "
            + "This book is a treatise on the theory of ethics, very popular during the Renaissance."
            + " The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.";

    public void showDialof() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setContentText("PDF Generated");
        alert.showAndWait();
    }

    public void writeFile(Document document) throws IOException {
        final OutputStream outputStream
                = new FileOutputStream("bnp-pdf.pdf");
        document.save(outputStream);
    }

    public void addHeader(Document document) throws IOException {
        Paragraph paragraph = new Paragraph();
        paragraph.addMarkup("{color:#000000}*Ain't no rectangle*", 22, BaseFont.Helvetica);
        paragraph.setAlignment(Alignment.Left);
        Frame frame = new Frame(paragraph, 525f, 37f);
        frame.setShape(new Rect());
        frame.setBorder(Color.BLACK, new Stroke(2));
        frame.setBackgroundColor(Color.decode("#e5fef4"));
        frame.setPadding(15, 0, 5, 0);
        document.add(frame, VerticalLayoutHint.LEFT);
    }

    public void getParagraph1(Document document, String Text, int fontSize, BaseFont baseFont, Alignment alignment,
            float width, float hieght, int m_left, int m_right, int m_top, int m_bottom, int p_left,
            int p_right, int p_top, int p_bottom, VerticalLayoutHint LEFT) throws IOException {
        Paragraph paragraph = new Paragraph();
        paragraph.addMarkup("{color:#000000}"+Text, fontSize, baseFont);
        paragraph.setAlignment(alignment);
        Frame frame = new Frame(paragraph, width, hieght);
        frame.setShape(new RoundRect(10));
        frame.setMargin(m_left, m_right, m_top, m_bottom);
        frame.setBorder(Color.decode("#e5fef4"), new Stroke(2));
        frame.setBackgroundColor(Color.decode("#e5fef4"));
        frame.setPadding(p_left, p_right, p_top, p_bottom);
        document.add(frame, LEFT);
    }

   

}
