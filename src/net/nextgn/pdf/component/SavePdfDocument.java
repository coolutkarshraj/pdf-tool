/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nextgn.pdf.component;

import net.nextgn.pdf.Models.NDocumentModel;
import net.nextgn.pdf.Models.NPages;
import net.nextgn.pdf.Pages.Page1.FXMLFirstPageTemplateController;
import net.nextgn.pdf.Pages.Page2.FXMLSecondPageControllerController;

/**
 *
 * @author NEXTGN001
 */
public class SavePdfDocument {

    public void saveDatatoNDocumentModelFirstPage() {
        NDocumentModel dm = new NDocumentModel();
        dm.jfx_content_one = FXMLFirstPageTemplateController.content_one;
        dm.jfx_content_two = FXMLFirstPageTemplateController.content_two;
        dm.jfx_content_three = FXMLFirstPageTemplateController.content_three;
        dm.jfx_content_four = FXMLFirstPageTemplateController.content_four;
        dm.header1 = FXMLFirstPageTemplateController.header;
        NPages.nDocument.add(dm);
        System.out.println("model" + dm);

    }

    public void saveDatatoNDocumentModelSecondPage() {
        NDocumentModel dm = new NDocumentModel();
        dm.second_page_content_one = FXMLSecondPageControllerController.content_one;
        dm.second_page_content_two = FXMLSecondPageControllerController.content_two;
        dm.second_page_content_three = FXMLSecondPageControllerController.content_three;
        dm.header_second_Page = FXMLSecondPageControllerController.header;
        NPages.nDocument.add(dm);
        System.out.println("model" + dm);
        int j = NPages.nDocument.size();

    }
}
