package net.nextgn.pdf.component;

import com.jfoenix.controls.JFXTextArea;
import java.io.File;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import net.nextgn.pdf.Pages.Page1.FxmlMainPageContent;

/**
 *
 * @author NEXTGN001
 */
public class ListOfComponent {

    public ImageView getImageView(ImageView imageView, int hieght, int width, int x, int y) {
        ImageView uploadedImageView_first_content = imageView;
        uploadedImageView_first_content.setFitHeight(hieght);
        uploadedImageView_first_content.setFitWidth(width);
        uploadedImageView_first_content.setX(x);
        uploadedImageView_first_content.setY(y);
        return uploadedImageView_first_content;
    }

    public Rectangle AddRectangle(int hieght, int width, int x, int y) {
        Rectangle rect = new Rectangle();
        rect.setHeight(hieght);
        rect.setWidth(width);
        rect.setFill(Color.WHITE);
        rect.setStroke(Color.web("#00685a"));
        rect.setX(x);
        rect.setY(y);
        return rect;
    }

    public ImageView AddImageView(File file, int hieght, int width, int x, int y) {
        FxmlMainPageContent mainPageContent = new FxmlMainPageContent();
        Image image = new Image(file.toURI().toString());
        mainPageContent.AddIconWidget = new ImageView(image);
        mainPageContent.AddIconWidget.setFitHeight(hieght);
        mainPageContent.AddIconWidget.setFitWidth(width);
        mainPageContent.AddIconWidget.setX(x);
        mainPageContent.AddIconWidget.setY(y);
        return mainPageContent.AddIconWidget;
    }

    public Label AddLabel(String font, int fontSize, String color, String labelText, int width, int hieght, int x, String par4, int y) {
        Label label = new Label();
        label.setFont(new Font(font, fontSize));
        label.setTextFill(Color.web(color));
        label.setText(labelText);
        label.setMaxWidth(width);
        label.setMaxHeight(hieght);
        label.setTranslateX(x);
        label.setStyle(par4);
        label.setTranslateY(y);
        return label;
    }

    public JFXTextArea addJFXTextArea(int hieght, int width, int x, int y) {
        JFXTextArea textarea = new JFXTextArea();
        textarea.setPrefHeight(hieght);
        textarea.setPrefWidth(width);
        textarea.setLayoutX(x);
        textarea.setLayoutY(y);
        return textarea;
    }

}
