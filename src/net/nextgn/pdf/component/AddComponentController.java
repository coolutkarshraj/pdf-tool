package net.nextgn.pdf.component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.jfoenix.controls.JFXTextArea;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser.ExtensionFilter;
import net.nextgn.pdf.Models.Constant;
import net.nextgn.pdf.Pages.Page1.FxmlMainPageContent;
import rst.pdfbox.layout.text.Constants;

public class AddComponentController implements Initializable {
    
   public static int position = 0 ;
   
   EventBus eventBusImage,eventBusText;
   FxmlMainPageContent listenerImage,listnerText;
           
    @FXML
    private Label jfl_deleteall;
    @FXML
    private Rectangle jfl_text;
    @FXML
    private Rectangle jfl_image;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
           eventBusImage = new EventBus();
           listenerImage = new FxmlMainPageContent();
           eventBusImage.register(listenerImage);
           eventBusText = new EventBus();
           listnerText = new FxmlMainPageContent();
           eventBusText.register(listnerText);
        // TODO
    }  
    
       @FXML
    void add_text(javafx.scene.input.MouseEvent event) throws IOException {
      Stage stage = (Stage) jfl_text.getScene().getWindow();
      eventBusText.post("componentAddedText");
      stage.close();
    }

    @FXML
    void add_image(javafx.scene.input.MouseEvent event) {
      FileChooser fc = new FileChooser();
  
      fc.getExtensionFilters().addAll(
      new ExtensionFilter("Png,jpg file", "*.png", "*.jpg"));
      File selectedFile = fc.showOpenDialog(null);
      
      if (selectedFile != null) {
          Constant.img = selectedFile.getPath();
           File file = new File(Constant.img);
           Image image = new Image(file.toURI().toString());
           ImageView iv = new ImageView(image);
           Constant.imageView =iv;
           System.out.println(Constant.imageView);
           Stage stage = (Stage) jfl_image.getScene().getWindow();
           stage.close();
           eventBusImage.post("componentAddedImage");
           System.out.println("Post Simple EventBus Example");
      } else
          {
          System.out.println("file is not valid");
            Stage stage = (Stage) jfl_image.getScene().getWindow();
           stage.close();
           eventBusImage.post("nonImage");
           System.out.println("Post Simple EventBus Example---------------");
        } 
   }
   

 }
