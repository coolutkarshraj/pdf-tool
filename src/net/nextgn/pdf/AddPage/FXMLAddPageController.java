/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nextgn.pdf.AddPage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author NEXTGN001
 */
public class FXMLAddPageController implements Initializable {
    @FXML
    Button btn_cancel;
  

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
     @FXML
    void cancelPage(ActionEvent event) throws IOException, IOException {
        System.out.println("close button clicked");
          Stage stage = (Stage) btn_cancel.getScene().getWindow();
          stage.close();
    }
     @FXML
    void savePage(ActionEvent event) throws IOException, IOException {
        System.out.println("close button clicked");
          Stage stage = (Stage) btn_cancel.getScene().getWindow();
          stage.close();
    }
}
