/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nextgn.pdf.Pages.Page2;

import com.jfoenix.controls.JFXTextArea;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import net.nextgn.pdf.Models.NDocumentModel;
import net.nextgn.pdf.Models.NPages;
import static net.nextgn.pdf.Pages.Page1.FXMLFirstPageTemplateController.content_four;
import static net.nextgn.pdf.Pages.Page1.FXMLFirstPageTemplateController.content_one;
import static net.nextgn.pdf.Pages.Page1.FXMLFirstPageTemplateController.content_three;
import static net.nextgn.pdf.Pages.Page1.FXMLFirstPageTemplateController.content_two;
import static net.nextgn.pdf.Pages.Page1.FXMLFirstPageTemplateController.header;

/**
 * FXML Controller class
 *
 * @author NEXTGN001
 */
public class FXMLSecondPageControllerController implements Initializable {
    public static String header,content_one,content_two,content_three; 
    @FXML
    private JFXTextArea second_page_content_one;

    @FXML
    private TextField header_second_Page;

    @FXML
    private JFXTextArea second_page_content_three;

    @FXML
    private JFXTextArea second_page_content_two;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
       @FXML
    void saveFile(javafx.scene.input.MouseEvent event) {
        content_one = second_page_content_one.getText();
        content_two = second_page_content_two.getText();
        content_three = second_page_content_three.getText();
        header = header_second_Page.getText();
    }
}
