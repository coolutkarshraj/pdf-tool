package net.nextgn.pdf.Pages.Page1;

import com.google.common.eventbus.Subscribe;
import com.jfoenix.controls.JFXTextArea;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import net.nextgn.pdf.Models.Constant;
import static net.nextgn.pdf.Pages.Page1.FXMLFirstPageTemplateController.textType;
import static net.nextgn.pdf.Pages.Page1.FXMLFirstPageTemplateController.updatePosition;
import net.nextgn.pdf.component.AddComponentController;
import net.nextgn.pdf.component.ListOfComponent;

/**
 *
 * @author NEXTGN001
 */
public class FxmlMainPageContent implements Initializable {

    static Rectangle rect_first_content_for_textView, rect_secondContent, rect_firsct_content_AddWidget, rect_second_content_for_textView;
    static JFXTextArea textarea_first_content, textarea_second_content;
    static ImageView imageView_delet_Image, imageView_delet_Image_secondContent, imageView_delet_Image_secondContent_text, imageView_delet_Image_on_imageView, plusIcon_secondContent, uploadedImage_1, Add_Widget_Image, uploadedImage_2;
    static Label add_label_widget_second_content, add_label_widget;
    static Pane pane;
    @FXML
    public TextArea textareahere;
    @FXML
    public Pane firstpane_pane;
    @FXML
    private Rectangle rect_first;
    @FXML
    public Rectangle rect_second;
    @FXML
    public ImageView plus_icon;
    @FXML
    private Label add_widget_label;
    @FXML
    private ImageView delete_image_first;
    public static int delete_position;
    public static int updatePosition = 0;
    public static int textType = 0;
    public ImageView deleteImage, AddIconWidget;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("I am update component");
        plusIcon_secondContent = plus_icon;
        rect_secondContent = rect_second;
        add_label_widget_second_content = add_widget_label;
        pane = firstpane_pane;
    }

    @FXML
    public void updateComponent(javafx.scene.input.MouseEvent event) throws IOException {
        delete_position = 2;
        Parent root = null;
        root = FXMLLoader.load(getClass().getResource("/net/nextgn/pdf/component/AddComponent.fxml"));
        Scene scene = new Scene(root, 507, 286);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void deleteContent_first(MouseEvent event) {
        ListOfComponent listOfComponent = new ListOfComponent();
        delete_position = 1;
        textareahere.setVisible(false);
        rect_first.setVisible(false);
        delete_image_first.setVisible(false);
        rect_firsct_content_AddWidget = listOfComponent.AddRectangle(559, 487, 59, 127);
        File file = new File("src\\net\\nextgn\\assets\\plus.png");
        Add_Widget_Image = listOfComponent.AddImageView(file, 31, 31, 261, 334);
        add_label_widget = listOfComponent.AddLabel("System", 24, "#d8d0d0", "ADD WIDGET", 228, 41, 216, "Regular", 376);
        pane.getChildren().addAll(rect_firsct_content_AddWidget, Add_Widget_Image, add_label_widget);
        rect_firsct_content_AddWidget.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                openNewController();
            }

            private void openNewController() {
                System.out.println("Tile pressed ");
                Parent root = null;
                try {
                    root = FXMLLoader.load(getClass().getResource("/net/nextgn/pdf/component/AddComponent.fxml"));
                } catch (IOException ex) {
                    Logger.getLogger(FxmlMainPageContent.class.getName()).log(Level.SEVERE, null, ex);
                }
                Scene scene = new Scene(root, 507, 286);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.show();
            }
        });
    }

    @Subscribe
    public void stringEvent(String event) {
        int textSelectedorImageSelected = delete_position;
        if (textSelectedorImageSelected == 1) {
            addTextorImageInFirstContent(event);

        } else if (textSelectedorImageSelected == 2) {
            addTextorImageInSecondContent(event);

        }

    }

    private void addImageInFirstContent() {
        ListOfComponent listOfComponent = new ListOfComponent();
        rect_firsct_content_AddWidget.setVisible(false);
        Add_Widget_Image.setVisible(false);
        add_label_widget.setVisible(false);
        uploadedImage_1 = listOfComponent.getImageView(Constant.imageView, 559, 487, 59, 127);
        File file = new File("src\\net\\nextgn\\assets\\delete.png");
        imageView_delet_Image = listOfComponent.AddImageView(file, 23, 23, 535, 116);
        pane.getChildren().addAll(uploadedImage_1, imageView_delet_Image);
        imageView_delet_Image.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                uploadedImage_1.setVisible(false);
                imageView_delet_Image.setVisible(false);
                rect_firsct_content_AddWidget.setVisible(true);
                Add_Widget_Image.setVisible(true);
                add_label_widget.setVisible(true);

            }
        });
    }

    private void addTextInFirstContent() {
        ListOfComponent listOfComponent = new ListOfComponent();
        rect_firsct_content_AddWidget.setVisible(false);
        Add_Widget_Image.setVisible(false);
        add_label_widget.setVisible(false);
        File file = new File("src\\net\\nextgn\\assets\\delete.png");
        rect_first_content_for_textView = listOfComponent.AddRectangle(559, 487, 59, 127);
        textarea_first_content = listOfComponent.addJFXTextArea(550, 478, 66, 130);
        ImageView imageView_delet_Imag = listOfComponent.AddImageView(file, 23, 23, 535, 116);
        imageView_delet_Image = imageView_delet_Imag;
        pane.getChildren().addAll(rect_first_content_for_textView, textarea_first_content, imageView_delet_Image);
        imageView_delet_Image.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                imageView_delet_Image.setVisible(false);
                textarea_first_content.setVisible(false);
                rect_first_content_for_textView.setVisible(false);
                rect_firsct_content_AddWidget.setVisible(true);
                Add_Widget_Image.setVisible(true);
                add_label_widget.setVisible(true);
            }
        });
    }

    private void addImageInSecondContent() {
        ListOfComponent listOfComponent = new ListOfComponent();
        plusIcon_secondContent.setVisible(false);
        rect_secondContent.setVisible(false);
        add_label_widget_second_content.setVisible(false);
        uploadedImage_2 = listOfComponent.getImageView(Constant.imageView, 559, 487, 671, 127);
        File file = new File("src\\net\\nextgn\\assets\\delete.png");
        ImageView imageView_delet_Image_secondConten = listOfComponent.AddImageView(file, 23, 23, 1147, 116);
        imageView_delet_Image_secondContent = imageView_delet_Image_secondConten;
        pane.getChildren().addAll(uploadedImage_2, imageView_delet_Image_secondContent);
        imageView_delet_Image_secondContent.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                uploadedImage_2.setVisible(false);
                imageView_delet_Image_secondContent.setVisible(false);
                plusIcon_secondContent.setVisible(true);
                rect_secondContent.setVisible(true);
                add_label_widget_second_content.setVisible(true);
            }
        });
    }

    private void addTextInSecondContent() {
        ListOfComponent listOfComponent = new ListOfComponent();
        plusIcon_secondContent.setVisible(false);
        rect_secondContent.setVisible(false);
        add_label_widget_second_content.setVisible(false);
        rect_second_content_for_textView = listOfComponent.AddRectangle(559, 487, 671, 127);
        textarea_second_content = listOfComponent.addJFXTextArea(550, 478, 671, 130);
        File file = new File("src\\net\\nextgn\\assets\\delete.png");
        ImageView imageView_delet_Imag = listOfComponent.AddImageView(file, 23, 23, 1147, 116);
        imageView_delet_Image_secondContent_text = imageView_delet_Imag;
        pane.getChildren().addAll(rect_second_content_for_textView, textarea_second_content, imageView_delet_Image_secondContent_text);
        imageView_delet_Image_secondContent_text.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                imageView_delet_Image_secondContent_text.setVisible(false);
                textarea_second_content.setVisible(false);
                rect_second_content_for_textView.setVisible(false);
                plusIcon_secondContent.setVisible(true);
                rect_secondContent.setVisible(true);
                add_label_widget_second_content.setVisible(true);

            }
        });
    }

    private void addTextorImageInFirstContent(String event) {
        if (event == "componentAddedText") {
            addTextInFirstContent();
        } else if (event == "componentAddedImage") {
            addImageInFirstContent();
        }
    }

    private void addTextorImageInSecondContent(String event) {
        if (event == "componentAddedText") {
            addTextInSecondContent();
        } else if (event == "componentAddedImage") {
            addImageInSecondContent();
        } else if (event == "nonImage") {
            AddImageview();

        }

    }

    private void AddImageview() {
        ListOfComponent listOfComponent = new ListOfComponent();
        plusIcon_secondContent.setVisible(false);
        rect_secondContent.setVisible(false);
        add_label_widget_second_content.setVisible(false);
        File file1 = new File("src\\net\\nextgn\\assets\\delete.png");
        ImageView imageView_delet_Image_secondConten = listOfComponent.AddImageView(file1, 23, 23, 1147, 116);
        imageView_delet_Image_secondContent = imageView_delet_Image_secondConten;
        ImageView imageView = new ImageView();
        File file = new File("src\\net\\nextgn\\assets\\drag.JPG");
        Image image = new Image(file.toURI().toString());
        imageView.setImage(image);
        imageView.setFitHeight(559);
        imageView.setFitWidth(487);
        imageView.setX(671);
        imageView.setY(127);
        imageView.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {

                if (event.getDragboard().hasFiles()) {
                    event.acceptTransferModes(TransferMode.ANY);
                    System.out.println("Frag detected");
                }

            }
        });
        imageView.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                 System.out.println("+++++++++++++++++++++++Post Simple EventBus Example---------------");
                    List<File> file = event.getDragboard().getFiles();
                try {
                    Image image = new Image(new FileInputStream(file.get(0)));
                    imageView.setImage(image);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(FxmlMainPageContent.class.getName()).log(Level.SEVERE, null, ex);
                }
             
            }
        });
         imageView_delet_Image_secondContent.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               imageView.setVisible(false);
               imageView_delet_Image_secondContent.setVisible(false);
               plusIcon_secondContent.setVisible(true);
               rect_secondContent.setVisible(true);
               add_label_widget_second_content.setVisible(true);
               System.out.println("delete component");

            }
        });
        pane.getChildren().addAll(imageView,imageView_delet_Image_secondContent);
    }
}
