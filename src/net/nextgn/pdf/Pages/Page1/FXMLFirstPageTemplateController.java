/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nextgn.pdf.Pages.Page1;

import com.jfoenix.controls.JFXTextArea;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import net.nextgn.pdf.Models.NDocumentModel;
import net.nextgn.pdf.Models.NPages;
import net.nextgn.pdf.component.AddComponentController;
/**
 * FXML Controller class
 *
 * @author administrator
 */
public class FXMLFirstPageTemplateController implements Initializable {
    public static int delete_position;
    public static int updatePosition = 0;
    public static int textType = 0;
    public static String header,content_one,content_two,content_three,content_four; 
 
    @FXML
    private JFXTextArea jfx_content_two;

    @FXML
    private TextField header1;

    @FXML
    private Rectangle main_header;

    @FXML
    private JFXTextArea jfx_content_four;

    @FXML
    private JFXTextArea jfx_content_three;

    @FXML
    private Pane firstpane_pane;

    @FXML
    private AnchorPane firstpane_anchor_pane;

    @FXML
    private JFXTextArea jfx_content_one;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    @FXML
    void saveText(javafx.scene.input.MouseEvent event) {
        content_one = jfx_content_one.getText();
        content_two = jfx_content_two.getText();
        content_three = header1.getText();
        content_four = jfx_content_two.getText();
        header = header1.getText();
    }
}
 
