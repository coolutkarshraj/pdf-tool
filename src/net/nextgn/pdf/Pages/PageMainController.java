package net.nextgn.pdf.Pages;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import net.nextgn.pdf.Models.NDocumentModel;
import net.nextgn.pdf.Models.NPages;
import net.nextgn.pdf.Test.Test1Controller;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import rst.pdfbox.layout.elements.Document;
import rst.pdfbox.layout.elements.Frame;
import rst.pdfbox.layout.elements.ImageElement;
import rst.pdfbox.layout.elements.Paragraph;
import rst.pdfbox.layout.elements.render.VerticalLayoutHint;
import rst.pdfbox.layout.shape.Rect;
import rst.pdfbox.layout.shape.Stroke;
import rst.pdfbox.layout.text.Alignment;
import rst.pdfbox.layout.text.BaseFont;
import rst.pdfbox.layout.text.Constants;
import rst.pdfbox.layout.text.Position;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import net.nextgn.pdf.Pages.Page1.FXMLFirstPageTemplateController;
import net.nextgn.pdf.Pages.Page2.FXMLSecondPageControllerController;
import net.nextgn.pdf.component.PdfGenerator;
import net.nextgn.pdf.component.SavePdfDocument;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;
import rst.pdfbox.layout.elements.Element;

public class PageMainController extends Application implements Initializable {

    @FXML
    private Pane firstPane;
    @FXML
    private ImageView addPage;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ScrollPane scroolpane;
    PDPageContentStream contentStream;
    String[] wrT = null;
    String s = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Pane newLoadedPane = null;
        try {
            newLoadedPane = FXMLLoader.load(getClass().getResource("Page1/FXMLMainPageContent.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        firstPane.getChildren().clear();
        firstPane.getChildren().add(newLoadedPane);
    }

    @FXML
    public void AddPageClicked(ActionEvent event) {
        SavePdfDocument savePdfDocument = new SavePdfDocument();
        Parent root = null;

        try {
            root = FXMLLoader.load(getClass().getResource("/net/nextgn/pdf/AddPage/FXMLAddPage.fxml"));
        } catch (IOException ex) {
            //
        }
        Scene scene = new Scene(root, 1000, 700);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
        savePdfDocument.saveDatatoNDocumentModelFirstPage();
        savePdfDocument.saveDatatoNDocumentModelSecondPage();

    }

    @FXML
    public void openFirstPage(javafx.scene.input.MouseEvent event) {
        SavePdfDocument savePdfDocument = new SavePdfDocument();
        Pane newLoadedPane = null;
        try {
            newLoadedPane = FXMLLoader.load(getClass().getResource("Page1/FXMLFirstPageTemplate.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        firstPane.getChildren().clear();
        firstPane.getChildren().add(newLoadedPane);
        savePdfDocument.saveDatatoNDocumentModelSecondPage();
    }

    @FXML
    public void openSecondPage(javafx.scene.input.MouseEvent event) {
        SavePdfDocument savePdfDocument = new SavePdfDocument();
        System.out.println("hello I Am Tap");
        Pane newLoadedPane = null;
        try {
            newLoadedPane = FXMLLoader.load(getClass().getResource("Page2/FXMLSecondPageController.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        firstPane.getChildren().clear();
        firstPane.getChildren().add(newLoadedPane);
        savePdfDocument.saveDatatoNDocumentModelFirstPage();
    }

    @FXML
    public void navigateToHome(MouseEvent mouseEvent) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/net/nextgn/pdf/Dashboard/Dashboard.fxml"));
        } catch (IOException ex) {
            //
        }

        Scene scene = new Scene(root, 1000, 700);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
        anchorPane.getScene().getWindow().hide();
    }

    @FXML
    private void generatePdf(ActionEvent event) throws IOException {
        Document document = new Document(Constants.A4, 40, 60, 40, 60);
        PdfGenerator pdfGenerator = new PdfGenerator();
        pdfGenerator.addHeader(document);
        pdfGenerator.getParagraph1(document, PdfGenerator.Text1, 12, BaseFont.Helvetica, Alignment.Left, 260f, 280f, 0, 0, 20, 0, 10, 0, 5, 0, VerticalLayoutHint.LEFT);
        pdfGenerator.getParagraph1(document, PdfGenerator.Text1, 12, BaseFont.Helvetica, Alignment.Left, 260f, 280f, 0, -35, -280, 0, 10, 0, 5, 0, VerticalLayoutHint.RIGHT);
        pdfGenerator.getParagraph1(document, PdfGenerator.Text1, 12, BaseFont.Helvetica, Alignment.Left, 260f, 280f, 0, 0, 20, 0, 10, 0, 5, 0, VerticalLayoutHint.LEFT);
        pdfGenerator.getParagraph1(document, PdfGenerator.Text1, 12, BaseFont.Helvetica, Alignment.Left, 260f, 280f, 0, -35, -280, 0, 10, 0, 5, 0, VerticalLayoutHint.RIGHT);
        pdfGenerator.writeFile(document);
        pdfGenerator.showDialof();

    }

    private void openFile() {
        File file = new File("bnp-pdf.pdf");
        HostServices hostServices = getHostServices();
        hostServices.showDocument(file.getAbsolutePath());
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
