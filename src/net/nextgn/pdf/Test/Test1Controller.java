/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nextgn.pdf.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import rst.pdfbox.layout.elements.Document;
import rst.pdfbox.layout.elements.Paragraph;
import rst.pdfbox.layout.elements.VerticalSpacer;
import rst.pdfbox.layout.elements.render.ColumnLayout;
import rst.pdfbox.layout.elements.render.VerticalLayoutHint;
import rst.pdfbox.layout.text.Alignment;
import rst.pdfbox.layout.text.BaseFont;
import rst.pdfbox.layout.text.Constants;
import rst.pdfbox.layout.text.Position;

/**
 * FXML Controller class
 *
 * @author administrator
 */
public class Test1Controller implements Initializable {
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private Button generatePDF;

    @FXML
    void createPDF(ActionEvent event) throws IOException {



        Document document = new Document(Constants.A4, 40, 60, 40, 60);

        System.out.println(document.getMediaBox().getHeight());
        System.out.println(document.getMediaBox().getWidth());

        String text1 = "This is me slightly longer text wrapped me slightly longer text wrapped me slightly longer text wrapped me slightly longer text wrapped me slightly longer text wrapped some slightly longer text wrapped to a width of 100.";

//        addTitle(document);

        document.add(getParagraph1(text1));
        document.add(getParagraph2(text1));

        writeFile(document);
    }

    private Paragraph getParagraph1(String text1) throws IOException {
        Paragraph paragraph1 = new Paragraph();
        paragraph1.addMarkup(text1, 11, BaseFont.Times);
        paragraph1.setAlignment(Alignment.Justify);
        paragraph1.setMaxWidth(200);
        paragraph1.setAbsolutePosition(new Position(40, 800));
        return paragraph1;
    }

    private Paragraph getParagraph2(String text1) throws IOException {
        Paragraph paragraph1 = new Paragraph();
        paragraph1.addMarkup(text1, 11, BaseFont.Times);
        paragraph1.setAlignment(Alignment.Justify);
        paragraph1.setMaxWidth(200);
        paragraph1.setAbsolutePosition(new Position(300, 800));
        return paragraph1;
    }

    private void writeFile(Document document) throws IOException {
        final OutputStream outputStream =
                new FileOutputStream("hellodoc.pdf");
        document.save(outputStream);
    }

    private void addTitle(Document document) throws IOException {
        Paragraph title = new Paragraph();
        title.addMarkup("*This Text is organized in Colums*",
                20, BaseFont.Times);
        document.add(title, VerticalLayoutHint.CENTER);
        document.add(new VerticalSpacer(5));
    }

    void createPDF1(ActionEvent event) throws IOException {
        PDDocument doc = null;
        try {
            doc = new PDDocument();
            PDPage page = new PDPage();
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            drawHeader(doc, contentStream);

            PDFont pdfFont = PDType1Font.HELVETICA;
            float fontSize = 10;
            float leading = 1.5f * fontSize;

            PDRectangle mediabox = page.getMediaBox();
            float margin = 192;
            float width = mediabox.getWidth() - 2 * margin;
            float startX = mediabox.getLowerLeftX() + margin;
            float startY = mediabox.getUpperRightY() - margin;

            String text = "Thomas \"Tom\" Cruise (born Thomas Cruise Mapother IV; July 3, 1962) is an American actor and producer. He has been nominated for three Academy Awards and has won three Golden Globe Awards. He started his career at age 19 in the film Endless Love.\n";


            ArrayList<String> lines = new ArrayList<>();
            int lastSpace = -1;
            while (text.length() > 0) {
                int spaceIndex = text.indexOf(' ', lastSpace + 1);
                if (spaceIndex < 0)
                    spaceIndex = text.length();
                String subString = text.substring(0, spaceIndex);
                float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
                System.out.printf("'%s' - %f of %f\n", subString, size, width);
                if (size > width) {
                    if (lastSpace < 0)
                        lastSpace = spaceIndex;
                    subString = text.substring(0, lastSpace);
                    lines.add(subString);
                    text = text.substring(lastSpace).trim();
                    System.out.printf("'%s' is line\n", subString);
                    lastSpace = -1;
                } else if (spaceIndex == text.length()) {
                    lines.add(text);
                    System.out.printf("'%s' is line\n", text);
                    text = "";
                } else {
                    lastSpace = spaceIndex;
                }
            }

            contentStream.drawLine(0, 50, 700, 50);

            contentStream.beginText();
            contentStream.setFont(pdfFont, 25);
            contentStream.newLineAtOffset(180, 750);
            contentStream.showText("US VS EU VOLATILITY VS CARRY");
            contentStream.endText();

            contentStream.beginText();
            contentStream.setFont(pdfFont, fontSize);
            contentStream.newLineAtOffset(20, startY);
            for (String line : lines) {
                contentStream.showText(line);
                contentStream.newLineAtOffset(0, -leading);
            }
            contentStream.endText();

            contentStream.beginText();
            contentStream.setFont(pdfFont, fontSize);
            contentStream.newLineAtOffset(320, startY);
            for (String line : lines) {
                contentStream.showText(line);
                contentStream.newLineAtOffset(0, -leading);
            }
            contentStream.endText();

            contentStream.beginText();
            contentStream.newLineAtOffset(125, 700);
            contentStream.setFont(pdfFont, 12);
            contentStream.showText("Tom Cruise");
            contentStream.close();


            doc.save(getFileName());
        } finally {
            if (doc != null) {
                doc.close();
            }
        }
    }

    private void drawHeader(PDDocument doc, PDPageContentStream contentStream) throws IOException {
        contentStream.drawLine(0, 718, 700, 718);
        try {
            String absolutePath = (new File("")).getAbsolutePath();
            absolutePath = absolutePath + "//src//net//nextgn//assets//header_c.png";
            PDImageXObject headerImage = PDImageXObject.createFromFile(absolutePath, doc);
            contentStream.drawImage(headerImage, 0, 720);
        } catch (Exception ex) {
            Logger.getLogger(Test1Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        String absolutePath = (new File("")).getAbsolutePath();
        absolutePath = absolutePath + "//src//net.nextgn.assets//profile.png";
        PDImageXObject headerImage = PDImageXObject.createFromFile(absolutePath, doc);
        contentStream.drawXObject(headerImage, 50, 680, 65, 75);

        String absolutePath1 = (new File("")).getAbsolutePath();
        absolutePath1 = absolutePath1 + "//src//net.nextgn.assets//bnp_logo.png";
        PDImageXObject logo = PDImageXObject.createFromFile(absolutePath1, doc);
        contentStream.drawXObject(logo, 50, 20, 85, 20);

    }

    private String getFileName() {
        Date date = new Date();
        int month = date.getMonth();
        int day = date.getDay();
        int year = date.getYear();
        int hours = date.getHours();
        int minutes = date.getMinutes();
        int seconds = date.getSeconds();
        return day + "-" + month + "-" + year + "-" + hours + "-" + minutes + "-" + seconds + ".pdf";

    }

}