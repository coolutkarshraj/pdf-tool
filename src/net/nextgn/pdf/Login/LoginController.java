
package net.nextgn.pdf.Login;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class LoginController implements Initializable {
    @FXML
    private AnchorPane loginPane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void login(ActionEvent event) throws IOException, IOException {
         Parent root = null;
          root = FXMLLoader.load(getClass().getResource("/net/nextgn/pdf/Dashboard/Dashboard.fxml"));
          Scene scene = new Scene(root,1680,1050);
          Stage stage = new Stage();
          stage.setScene(scene);
          stage.show();
          loginPane.getScene().getWindow().hide();
    }                              
}
