/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nextgn.pdf.Dashboard;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author NEXTGN001
 */
public class DashboardController implements Initializable {
    
    @FXML
    private AnchorPane landingPage;
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
     @FXML
    void openSinglePagePDf(javafx.scene.input.MouseEvent event) throws IOException, IOException {
       Parent root = null;
          root = FXMLLoader.load(getClass().getResource("/net/nextgn/pdf/Pages/PageMain.fxml"));
          Scene scene = new Scene(root,1680,1050);
          Stage stage = new Stage();
          stage.setScene(scene);
          stage.show();
          landingPage.getScene().getWindow().hide();
    }     
     @FXML
    void openMultiplePagePdf(javafx.scene.input.MouseEvent event) throws IOException, IOException {
     Parent root = null;
          root = FXMLLoader.load(getClass().getResource("/net/nextgn/pdf/Pages/PageMain.fxml"));
          Scene scene = new Scene(root,1680,1050);
          Stage stage = new Stage();
          stage.setScene(scene);
          stage.show();
          landingPage.getScene().getWindow().hide();
    }   
     
}
